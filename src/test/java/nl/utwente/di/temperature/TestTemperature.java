package nl.utwente.di.temperature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestTemperature {

    @Test
    public void testTemp1() throws Exception {
        Fahrenheit fahrenheit = new Fahrenheit();
        double degrees= fahrenheit.getCelsiusToFahrenheit(1);
        Assertions.assertEquals(33.8, degrees, 0.0, "1 Degrees Celsius to Fahrenheit");
    }
}
