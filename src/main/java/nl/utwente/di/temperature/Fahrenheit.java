package nl.utwente.di.temperature;

public class Fahrenheit {
    double getCelsiusToFahrenheit(double celsius) {
        return celsius / (double)5 * (double)9 + (double)32;
    }
}
