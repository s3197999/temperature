package nl.utwente.di.temperature;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

public class Temperature extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Fahrenheit fahrenheit;

    public void init() throws ServletException {
        fahrenheit = new Fahrenheit();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Celsius to Fahrenheit convertor";

        // Done with string concatenation only for the demo
        // Not expected to be done like this in the project
        out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Celsius: " +
                request.getParameter("temp") + "\n" +
                "  <P>Fahrenheit: " +
                fahrenheit.getCelsiusToFahrenheit(Integer.parseInt(request.getParameter("temp"))) +
                "</BODY></HTML>");
    }
}
